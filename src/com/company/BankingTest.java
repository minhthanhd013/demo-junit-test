package com.company;

import static org.junit.Assert.*;

public class BankingTest {

    @org.junit.Test
    public void deposit() {
        Banking account = new Banking("Tim","Buchaka",1000.00, Banking.CHECKING);
        double balance = account.deposit(200.00,true);
        assertEquals(1200.00, balance, 0) ;
        assertEquals(1200.00,account.getBalance(),0);

    }

    @org.junit.Test
    public void withdraw() {
        fail("This test has yet to be implemented");
    }
    @org.junit.Test
    public void getBalance_deposit() {
        Banking account = new Banking("Tim","Buchaka",1000.00, Banking.CHECKING);
        account.deposit(200.00,true);
        assertEquals(1200, account.getBalance(), 0) ;
    }
    @org.junit.Test
    public void getBalance_withdraw() {
        Banking account = new Banking("Tim","Buchaka",1000.00, Banking.CHECKING);
        account.withdraw(200.00,true);
        assertEquals(800, account.getBalance(), 0) ;
    }
    @org.junit.Test
    public void isChecking_true() {
        Banking account = new Banking("Tim","Buchaka",1000.00, Banking.CHECKING);
        assertEquals(true, account.isChecking());
    }

}
