package com.company;

public class Banking {
    private String firstName;
    private String lastName;
    private double balance;

    public static  final int CHECKING = 1;
    public static  final int SAVINGS = 2;

    private int accountType;


    public Banking(String firstName, String lastName, double balance, int typeofAccount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
        this.accountType = typeofAccount;
    }

    public double deposit(double amount, boolean branch){
        balance += amount;
        return balance;
    }
    public boolean isChecking(){
        return accountType == CHECKING;
    }

    public double withdraw(double amount, boolean branch){
        balance-= amount;
        return balance;
    }
    public double getBalance(){
        return balance;
    }
}
